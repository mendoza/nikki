import config
import discord
import io
import random
import requests

from discord.ext import commands
from PIL import Image, ImageDraw, ImageFont


class ShipCommand(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def create_card(self, member_a: discord.Member, member_b: discord.Member):
        mod = random.randrange(0, 100)

        img_height = 280
        img_width = 500
        font_size = 28

        member_a_avatar = Image.open(io.BytesIO(requests.get(
            member_a.avatar_url_as(format="png", size=128)).content))
        member_b_avatar = Image.open(io.BytesIO(requests.get(
            member_b.avatar_url_as(format="png", size=128)).content))

        card = Image.new(
                mode="RGB",
                size=(img_width, img_height),
                color=(255, 192, 203))
        i_ctx = ImageDraw.Draw(card)
        i_fnt = ImageFont.truetype(
                font="res/fonts/IndieFlower-Regular.ttf",
                size=font_size)

        card.paste(member_a_avatar, (30, 58))
        card.paste(member_b_avatar, ((img_width - member_b_avatar.width - 30), 58))

        i_ctx.text(
                xy=(40, 25),
                font=i_fnt,
                text=member_a.display_name,
                fill=(0, 15, 50))

        i_ctx.text(
                xy=((img_width - (round(28 / 2) * len(member_b.display_name)) - 40), 25),
                font=i_fnt,
                text=member_b.display_name,
                fill=(0, 15, 50))

        i_ctx.text(
                xy=(((img_width / 2) - round(28 / 2)), img_height - 50 - 38),
                font=i_fnt,
                text=f"{mod}%",
                fill=(0, 15, 50))

        i_ctx.rectangle(
                [(50, img_height - 20), (img_width - 50, img_height - 50)],
                fill=(40, 40, 60))

        i_ctx.rectangle(
                [(50, img_height - 20), (img_width - 50 - (((100 - mod) / 100) * 400), img_height - 50)],
                fill=(150, 255, 150))

        card.save("card.png", "PNG")

    @commands.command()
    async def ship(self, ctx, member_a: discord.Member = None, member_b: discord.Member = None):
        try:
            if member_a and member_b:
                self.create_card(member_a, member_b)
                await ctx.send(file=discord.File("card.png"))
            elif member_a and not member_b:
                self.create_card(ctx.author, member_a)
                await ctx.send(file=discord.File("card.png"))
            else:
                await ctx.send("Who are you shipping yourself to? Mention a member, please.")
        except commands.errors.MemberNotFound:
            await ctx.send("Please mention a member")


def setup(bot):
    bot.add_cog(ShipCommand(bot))
