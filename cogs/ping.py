import discord
import time

from discord.ext import commands


class PingCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def ping(self, ctx):
        ws_ping = round(self.client.latency * 1000)
        time_start = time.perf_counter()

        msg = await ctx.send("Pinging...")
        time_end = time.perf_counter()

        await msg.edit(content=f"**Ping:** {round(time_end - time_start) * 1000} | **Websocket:** {ws_ping}")


def setup(client):
    client.add_cog(PingCommand(client))
