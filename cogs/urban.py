import discord
import json
import requests

from discord.ext import commands


class UrbanCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def urban(self, ctx, term, index: int = None):
        data = requests.get(f"https://api.urbandictionary.com/v0/define?term={term}").json().get("list")

        if index is None:
            count = 1

            embed = discord.Embed(
                    title="Results",
                    color=0xFFA500)

            for i in data:
                embed.add_field(
                        name=f"{count}. {i['word']}",
                        value=i["definition"][0:50],
                        inline=False)

                count += 1

            await ctx.send(embed=embed)
        else:
            await ctx.send(f"*{data[index - 1]['word']}*: {data[index - 1]['definition']}")


def setup(client):
    client.add_cog(UrbanCommand(client))
