import config
import discord
import json
import random

from discord.ext import commands, tasks


class RandomMessage(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.send_random_message.start()
        self.last_quote = ""

    def cog_unload(self):
        self.send_random_message.cancel()

    @tasks.loop(hours=2.0)
    async def send_random_message(self):
        guild = await self.bot.fetch_guild(config.get("GUILD"))
        channel = await self.bot.fetch_channel(
                random.choice(json.loads(config.get("CHANNELS"))))
        all_members = await guild.fetch_members(limit=50).flatten()
        members = []
        quotes = []

        for member in all_members:
            if not member.bot:
                members.append(member)

        member_one = random.choice(members)
        member_two = random.choice(members)

        f = open("res/quotes.txt", 'r')
        quotes = f.readlines()
        f.close()

        quote = random.choice(quotes).replace('\n', '')

        await channel.send(quote.format(
            member_one=member_one.mention, member_two=member_two.mention))

    @send_random_message.before_loop
    async def wait_until_ready(self):
        await self.bot.wait_until_ready()


def setup(bot):
    bot.add_cog(RandomMessage(bot))
