import config
import discord
import os

from discord.ext import commands


def main():
    client = commands.Bot(
            config.get("PREFIX"),
            intents=discord.Intents().all())

    @client.event
    async def on_ready():
        print("Logged in!")
        await client.change_presence(
                activity=discord.Activity(
                    type=discord.ActivityType.watching,
                    name="these cuties~"))

    for f in os.listdir("./cogs"):
        if f.endswith(".py"):
            client.load_extension(f"cogs.{f[:-3]}")

    client.run(config.get("TOKEN"))


if __name__ == "__main__":
    main()
