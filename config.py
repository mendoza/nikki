import json
import os


def get(key):
    f = open("config.json", 'r')
    config = json.load(f)
    f.close()

    if config["USE_ENV_VARIABLES"]:
        return os.environ[key]
    else:
        return config[key]
